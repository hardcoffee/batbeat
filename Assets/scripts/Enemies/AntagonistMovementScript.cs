﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntagonistMovementScript : MonoBehaviour {

    private SpriteRenderer spriteRender;
    public float speed = 5f;
    public bool canMove = false;
    public int TimeToDestroy = 40;
    public float opacityObject = 1f;

    public float distanceMove = 20f;
    public float speedMove = 5f;
    public float timeToMove = 5f;
    private float auxMove = 0f;
    private enum directionMovement { up, down};
    directionMovement directionMove = directionMovement.up;

    public GameObject Player;

    private PlayerMovementScript _playerMovement;
    private PlayerScript _player;

    // Use this for initialization
    void Start() {
        spriteRender = GetComponent<SpriteRenderer>();

        if ( Player ) {
            _playerMovement = Player.GetComponent<PlayerMovementScript>();
            _player = Player.GetComponent<PlayerScript>();
        }

        if (opacityObject != 1 && opacityObject != 0)
        { 
            spriteRender.color = new Color(1f, 1f, 1f, opacityObject); 
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (canMove == false) {
            if (Player)
            {
                canMove = _playerMovement.objectsCanMove;
            }
        }

        if (Player.GetComponent<PlayerScript>().IsPlayerDead) // Se o player morreu, trava o darkBat na tela
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().useGravity = false;
        }

        // Se está na hora, ele move!
        auxMove += Time.deltaTime;
        if (auxMove >= timeToMove)
        {
            auxMove = 0;
            Jump();
        }

        if (canMove && !_player.IsDead())
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
            Destroy(this.gameObject, TimeToDestroy);
        }
	}

    public void Jump()
    {
        float step = speedMove * Time.deltaTime;
        float auxPointToMove = transform.position.y;
        if (directionMove == directionMovement.up)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, auxPointToMove+distanceMove, 0), step);
            //transform.Translate(new Vector3(1f, distanceMove * 2, 1f) * speedMove * Time.deltaTime);
            directionMove = directionMovement.down;
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, auxPointToMove- distanceMove*2, 0), step);
            //transform.Translate(new Vector3(1f, -distanceMove * 2, 1f) * speedMove * Time.deltaTime);
            directionMove = directionMovement.up;
        }
    }
}
