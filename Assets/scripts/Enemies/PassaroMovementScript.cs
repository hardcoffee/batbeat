﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassaroMovementScript : MonoBehaviour {

    private SpriteRenderer spriteRender;
    public float speed = 5f;
    public bool canMove = false;
    public int TimeToDestroy = 40;
    public float opacityObject = 1f;

    public GameObject Player;

    private PlayerMovementScript _playerMovement;
    private PlayerScript _player;

    // Use this for initialization
    void Start() {
        spriteRender = GetComponent<SpriteRenderer>();

        if ( Player ) {
            _playerMovement = Player.GetComponent<PlayerMovementScript>();
            _player = Player.GetComponent<PlayerScript>();
        }

        if (opacityObject != 1 && opacityObject != 0)
        { 
            spriteRender.color = new Color(1f, 1f, 1f, opacityObject); 
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (canMove == false) {
            if (Player)
            {
                canMove = _playerMovement.objectsCanMove;
            }
        }

        if (canMove && !_player.IsDead())
        {
            transform.Translate(-Vector3.right * speed * Time.deltaTime);
            Destroy(this.gameObject, TimeToDestroy);
        }
	}
}
