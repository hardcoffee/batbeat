﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovementScript : MonoBehaviour
{
    public SceneCtrl sceneCtrl; // Controller para pegar segundos e mili da fase
    public float _addJumpPoint = 3f; // Ponto limite de salto do peixe
    private float _roofPoint; // Ponto limite de salto do peixe
    public float moveSpeed = 3f; // Velocidade do salto
    private float _startPosition; // Posição inicial do peixe no mundo
    private float _newPosition; // Posição atual do peixe no mundo
    public float timeStartJump = 0f; // Tempo para que o peixe inicie seus saltos
    private bool _isGoingUp = true;
    private bool _isJumping = false;
    public bool canMove = false;
    public bool animationDirection = true;
    public GameObject Player; // Morcego
    Rigidbody rb;

    private float _sumTimeBetweenJump = 0f; // Somatória para controlar tempo entre saltos
    private bool _canJump = true;
    public float secondsBetweenJump = 1f; // Tempo para que o peixe inicie seus saltos

    private PlayerMovementScript _playerMovement;
    private PlayerScript _player;
    private FishSplashController _fishSplash;
    private Animator _fishAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _startPosition = transform.position.y; // Ponto atual do peixe no mundo
        _newPosition = _startPosition;
        _roofPoint = _newPosition + _addJumpPoint;
        _sumTimeBetweenJump = sceneCtrl.seconds; // Inicialmente, recebe o tempo atual da fase
        rb = GetComponent<Rigidbody>();
        _fishSplash = GetComponentInChildren<FishSplashController>();
        _fishAnimator = GetComponent<Animator>();

        if (Player)
        {
            _playerMovement = Player.GetComponent<PlayerMovementScript>();
            _player = Player.GetComponent<PlayerScript>();
        }
    }

    // Movimento de salto - estável independente da plataforma pelo fixed
    void FixedUpdate()
    {
        if (canMove == false)
        {
            // Enquanto não pode mover, o Rigidibody nao move
            rb.useGravity = false;
            if (Player)
            {
                canMove = _playerMovement.objectsCanMove;
                //if (canMove) { gameObject.GetComponent<Animator>().SetBool("walk", true); } // Animação de caminhar 
            }
        }
        if (!Player.GetComponent<PlayerScript>().IsPlayerDead) // Se o player não morreu, pode mover na tela
        {
            if (sceneCtrl.seconds >= timeStartJump)
            {
                if (!_isJumping && !_fishSplash.SplashRuning && (_sumTimeBetweenJump <= sceneCtrl.seconds) ) // Verifica se pode liberar o salto
                {
                    _fishSplash.StartSplash();
                }

                if (_canJump && canMove && !_fishSplash.SplashRuning) // Fica verificando se pode executar o salto
                {
                    // Se está subindo, adiciona posição Y. Se está descendo, retira posição Y
                    if (_isGoingUp)
                    {
                        _newPosition += moveSpeed * Time.deltaTime;
                    }
                    else
                    {
                        _newPosition -= moveSpeed * Time.deltaTime;
                    }
                    transform.position = new Vector3(transform.position.x, _newPosition, transform.position.z); // Movimenta
                    // Verifica os limites de movimentação
                    if (transform.position.y >= _roofPoint) // Se chegou no teto, para de subir e desce
                    {
                        _isGoingUp = false;
                        DownAnimation();
                    }
                    else if (transform.position.y <= _startPosition) // Se desceu ao chão, ele sobe
                    {
                        _isGoingUp = true;
                        if (_isJumping) // Se estava saltando, então para o salto e aguarda o tempo definido entre saltos
                        {
                            _sumTimeBetweenJump = sceneCtrl.seconds + secondsBetweenJump; // Atualiza a posição de controle do salto
                            _canJump = false; // Manda parar o salto para verificar o tempo entre saltos
                            _isJumping = false; // Informa que concluiu o salto
                            _fishSplash.EndSplash();
                            UpAnimation();
                        }
                    }
                }

            }
        }
    }

    public void Jump()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        _isJumping = true;
        _canJump = true;
    }

    public void StopJump()
    {
        _canJump = false;
    }

    public void UpAnimation()
    {
        _fishAnimator.SetTrigger("Up");
        animationDirection = true;
    }

    public void DownAnimation()
    {
        _fishAnimator.SetTrigger("Down");
        animationDirection = false;
    }
}
