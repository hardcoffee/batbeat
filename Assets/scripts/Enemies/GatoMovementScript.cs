﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatoMovementScript : MonoBehaviour {

    // Coropo e variaveis de movimentacao
    private SpriteRenderer spriteRender;
    Vector3 velocity = Vector3.zero;
    public GameObject Player; // Morcego
    public SceneCtrl sceneCtrl; // Controller para pegar segundos e mili da fase
    private AudioSource AudioSource;
    Rigidbody rb;

    public AudioClip CatMeow;
    public AudioClip catJump;
    public bool canMove = false;
    public int TimeToDestroy = 40;
    public float opacityObject = 1f;

    public float jump = 2200f;
    private bool ja_moveu = false;
    private float auxMeow = 0f;
    public float timeJump = 0f;
    public float milisecondJump = 0f;
    public float speed = 5f;

    private PlayerMovementScript _playerMovement;
    private PlayerScript _player;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.right * speed);
        AudioSource = GetComponent<AudioSource>();

        spriteRender = GetComponent<SpriteRenderer>();

        if ( Player ) {
            _playerMovement = Player.GetComponent<PlayerMovementScript>();
            _player = Player.GetComponent<PlayerScript>();
        }
        if (opacityObject != 1 && opacityObject != 0)
        {
            spriteRender.color = new Color(1f, 1f, 1f, opacityObject);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canMove == false)
        {
            // Enquanto não pode mover, o Rigidibody nao move
            rb.useGravity = false;
            if (Player)
            {
                canMove = _playerMovement.objectsCanMove;
                if (canMove) { gameObject.GetComponent<Animator>().SetBool("walk", true); } // Animação de caminhar 
            }
        }
        if (Player.GetComponent<PlayerScript>().IsPlayerDead) // Se o player morreu, trava o gato na tela
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().useGravity = false;
        }

        if ( canMove && !_player.IsDead())
        {
            // Movimenta
            velocity.x = speed;
            transform.position -= velocity * Time.deltaTime;
            //==================================================
            if (!ja_moveu)
            {
                // Se ainda não pulou, fica miando
                // Só começa a miar quando faltar 3 segundos para pular
                auxMeow += Time.deltaTime;
                if (auxMeow >= 2 && auxMeow >= timeJump - 3f)
                {
                    AudioSource.clip = CatMeow;
                    AudioSource.Play();
                    auxMeow = 0;
                }

                // Se está na hora, ele pula!
                if(sceneCtrl.seconds > timeJump && sceneCtrl.milliseconds > milisecondJump)
                {
                    gameObject.GetComponent<Animator>().SetTrigger("jump"); // Animação do salto
                    AudioSource.clip = catJump;
                    AudioSource.Play();
                    Jump();
                    rb.useGravity = true;
                    ja_moveu = true;
                }
            }
            //==================================================
            Destroy(this.gameObject, TimeToDestroy);
        }
    }
    public void Jump()
    {
        rb.velocity = Vector3.zero;
        rb.AddForce(transform.up * jump);
    }
    // Colisão com outros elementos 2D
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Footer")
        {
            rb.velocity = Vector3.zero;
            rb.useGravity = false;
            gameObject.GetComponent<Animator>().SetTrigger("landing"); // Animação do salto
        }
    }
}
