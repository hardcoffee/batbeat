using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSplashController : MonoBehaviour
{
    private FishMovementScript _fish = new FishMovementScript();
    private Animator _fishSplashAnimator;
    public bool SplashRuning = false;

    // Start is called before the first frame update
    void Start()
    {
        _fish = GetComponentInParent<FishMovementScript>();
        _fishSplashAnimator = GetComponent<Animator>();
    }

    /*
    1 - ativa a anima��o do splash do peixe
    2 - a pr�pria anima��o aciona o salto e anima��o do peixe
    3 - ao final da anima�ao do peixe, aciona a anima��o down do splash
    */
    public void StartSplash()
    {
        if (!SplashRuning)
            SplashRuning = true;
            _fishSplashAnimator.SetTrigger("Jump");
    }

    public void StartFishJump()
    {
        SplashRuning = false;
        GetComponent<SpriteRenderer>().enabled = false;
        _fish.Jump();
    }

    public void EndSplash()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        _fish.GetComponent<SpriteRenderer>().enabled = false;
        _fishSplashAnimator.SetTrigger("Fall");
    }
}
