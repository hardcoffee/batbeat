using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinScript : MonoBehaviour
{
    public int giroX = 0;
    public int giroY = 0;
    public int giroZ = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(giroX, giroY, giroZ);
    }
}
