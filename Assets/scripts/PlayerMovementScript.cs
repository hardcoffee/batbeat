﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {

    Vector3 velocity = Vector3.zero;
    Rigidbody rb;
    public float jump = 600f;
    public float speed = 5f;
    public bool objectsCanMove = false;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.right*speed);
	}
	// Graphics and Input updates
	void Update () {
       
        // Pega o botão
        if (Input.GetKeyDown(KeyCode.Space) && (Time.timeScale==1) )
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(transform.up * jump);
            // Indica que os bichos podem se mover
            objectsCanMove = true;
        }
        // Touch na tela
        if ( (Input.touchCount > 0) && (Time.timeScale == 1) )
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(transform.up * jump);
            // Indica que os bichos podem se mover
            objectsCanMove = true;
        }
	}
    // Physics engine updates
    void FixedUpdate()
    {
        velocity.x = speed;
        transform.position += velocity * Time.deltaTime;
    }
}
