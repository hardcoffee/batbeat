﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracksScript : MonoBehaviour {

    public GameObject PlayerObject;
    public Transform desiredBorderPosition;

    Transform player;
    float offSetx;

	// Use this for initialization
	void Start () {
        GameObject player_go = PlayerObject;

        if (player_go == null)
        {
            Debug.LogError("não achei o objeto");
            return;
        }
        player = player_go.transform;
        offSetx = transform.position.x - player.position.x;

        Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        float leftBorderPosition = transform.position.x - (stageDimensions.x - transform.position.x);

        if ( leftBorderPosition > desiredBorderPosition.position.x ) {
            offSetx -= leftBorderPosition - desiredBorderPosition.position.x;
        } else {
            offSetx += desiredBorderPosition.position.x - leftBorderPosition;
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (player != null)
        {
            Vector3 pos = transform.position;
            pos.x = player.position.x + offSetx;
            transform.position = pos;
        }
	}
}
