﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

    // VARIAVEIS
    public int language = 1; // 0 = Portugues | 1 = Inglês | 2 = Alemão
    public float volume = 1f;
    // CONFIGURAÇÕES
    private string[,] matr_texts;

    public static GameManagerScript Instance { get; private set; }
    // Primeira instancia geral, não permitindo outras instancias
    private void Awake () {
        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Define o idioma | Se tiver sido definido antes, pega. Senão, define
        if (PlayerPrefs.GetInt("language", 0)>0){language = PlayerPrefs.GetInt("language"); } else{ PlayerPrefs.SetInt("language", language); }
        // Tratamento de idiomas
        matr_texts = new string[3, 10];
        matr_texts[0,0] = "Ajude o morcego a chegar na caverna!";
        matr_texts[1,0] = "Help the bat to find the cave!";
    }

    public bool VerifyFase(int fase)
    {
        int check_fase = PlayerPrefs.GetInt("fase_done_" + fase, 0);
        if (check_fase == 0) { return false; } else { return true; }
    }

    public void CheckFase(int fase)
    {
        PlayerPrefs.SetInt("fase_done_" + fase, 1);
    }
}
