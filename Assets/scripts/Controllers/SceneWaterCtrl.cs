﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneWaterCtrl : MonoBehaviour {

    //=================//
    // ** OBJECTS ** //
    //===============//
    public GameObject MaskWater;
    public PlayerScript PlayerScriptScene;
    //=================//
    // ** VARIABLES ** //
    //===============//
    
    // Verifica na cena o tempo todo se o morcego morreu ou chegou no final para desabilitar a máscara do reflexo
    // Verifica em tempo real a posição do morcego, pra controlar a posição do reflexo
    void FixedUpdate()
    {
        // Habilita ou desabilita a máscara 
       if (PlayerScriptScene.IsPlayerDead)
        {
            Disable_Water_Mask();
        }
        if (PlayerScriptScene.HasPlayerWon)
        {
            Disable_Water_Mask();
        }


    }


    void Disable_Water_Mask () {
        MaskWater.SetActive(false);
    }

}
