﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuCtrl : MonoBehaviour {

    //public GameObject aboutMenu, configMenu, helpMenu, choiceMenu, GameManager;
    public AudioSource clicaNoBotao, passaOMouse;
    public GameObject[] Botoes_Fases = new GameObject[8];

    private void Start(){
		if (PlayerPrefs.HasKey("AudioListener"))
		{
			float volumeSlider = PlayerPrefs.GetFloat("AudioListener");
			AudioListener.volume = volumeSlider;
		} else
        {
			AudioListener.volume = 0.5f;
		}
		// Verifica as fases que o jogador já desabilitou
		DeactivePhases();
		print(GameManagerScript.Instance.VerifyFase(1));
		if (GameManagerScript.Instance.VerifyFase(1)) { Debug.Log(1); Botoes_Fases[1].gameObject.SetActive(true); }
        if (GameManagerScript.Instance.VerifyFase(2)) { Debug.Log(2); Botoes_Fases[2].gameObject.SetActive(true); }
        if (GameManagerScript.Instance.VerifyFase(3)) { Debug.Log(3); Botoes_Fases[3].gameObject.SetActive(true); }
		if (GameManagerScript.Instance.VerifyFase(4)) { Debug.Log(4); Botoes_Fases[4].gameObject.SetActive(true); }
		if (GameManagerScript.Instance.VerifyFase(5)) { Debug.Log(5); Botoes_Fases[5].gameObject.SetActive(true); }
		if (GameManagerScript.Instance.VerifyFase(6)) { Debug.Log(6); Botoes_Fases[6].gameObject.SetActive(true); }
		if (GameManagerScript.Instance.VerifyFase(7)) { Debug.Log(7); Botoes_Fases[7].gameObject.SetActive(true); }
	}

	private void DeactivePhases()
    {
		Botoes_Fases[0].SetActive(true);
		Botoes_Fases[1].SetActive(false);
		Botoes_Fases[2].SetActive(false);
		Botoes_Fases[3].SetActive(false);
		Botoes_Fases[4].SetActive(false);
		Botoes_Fases[5].SetActive(false);
		Botoes_Fases[6].SetActive(false);
		Botoes_Fases[7].SetActive(false);
	}

	public void LoadScene(string sceneName){
		SceneManager.LoadScene (sceneName);
	}


	public void SetVolume(Slider volumeSlider){
		AudioListener.volume = volumeSlider.value;
		PlayerPrefs.SetFloat("AudioListener", volumeSlider.value);
	}

	public void SetTextureQuality(Slider textureQualitySlider) {
		QualitySettings.globalTextureMipmapLimit = (int)textureQualitySlider.value; 
	}

	public void SetVSync(Slider vSyncSlider) {
		QualitySettings.globalTextureMipmapLimit = (int)vSyncSlider.value; 
	}

	public void CloseApp(){
		Application.Quit();
	}

    // Evento de click no botão
    public void ClickButtonSound()
    {
        clicaNoBotao.volume = 0.08f;
        clicaNoBotao.Play();
    }

    // Evento de click no botão
    public void HoverButtonSound()
    {
        passaOMouse.volume = 0.08f;
        passaOMouse.Play();
    }
}
