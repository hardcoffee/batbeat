﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneCtrl : MonoBehaviour {

    //=================//
    // ** OBJECTS ** //
    //===============//
    public GameObject Player;
    public GameObject ImageTouch;
    //=================//
    // ** VARIABLES ** //
    //===============//
    private bool playerMoving = false; // Começa falso pois o player começa travado
    public bool inTutorial = true; // Diz se o jogador clicou na tela 1 vez / para nao chamar a musica/movimento mais de 1 vez
    
    private float minutes = 0.0f;
    public float seconds = 0.0f;
    public float milliseconds = 0.0f;
    private float secondsTotal = 0.0f;
    //=================//
    // ** SOUNDS ** //
    //===============//
    private AudioSource audioSource;
    public AudioClip PhaseMusic;
    //=================//
    // ** GENERAL ** //
    //===============//
    [System.Serializable]
    public struct GroupsEnemies { public GameObject enemies; public float secondActive; public float secondDestroy; }
    [System.Serializable]
    public struct TimerPlaySonar { public float seconds; public float miliseconds; public bool hasPlayed; };
    public TimerPlaySonar[] arrayTimers;
    public GroupsEnemies[] arrayEnemies;
    public Text timer;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Inicialmente o player deve ficar parado (Não move e também não cai)
        Player.GetComponent<PlayerMovementScript>().enabled = false;
        Player.GetComponent<Rigidbody>().useGravity = false;
        // Chama o radar repetidamente
        InvokeRepeating("PlaySonar", 0, 3.0f);

        // PERCORRE array de Inimigos para desativar todos inicialmente
        if (arrayEnemies.Length > 0)
        {
            for (int j = 0; j < arrayEnemies.Length; j++)
            {
                /*/ Exceto o primeiro
                if (j != 0)
                {*/
                    //arrayEnemies[j].enemies.SetActive(true); // Para efeito de testes, ativa todo mundo pra testar o desempenho
                    arrayEnemies[j].enemies.SetActive(false); // Desativa todo mundo
                //}
            }
        }

    }

    void FixedUpdate()
    {
        // Ativa o radar de acordo com a música tema da fase
        if (playerMoving)
        {
            // SONAR (CONTADOR DE TEMPO)
            milliseconds += Time.deltaTime * 1000;
            if (milliseconds > 999)
            {
                seconds += 1;
                milliseconds = 0;
                secondsTotal += 1;
            }
            if (seconds > 59)
            {
                minutes += 1;
                seconds = 0;
            }
            milliseconds = Mathf.Round(milliseconds);
            if (timer)
            {
                timer.text = seconds.ToString() + " " + milliseconds.ToString();
            }

            // PERCORRE array de emitir sonar
            for (int i = 0; i < arrayTimers.Length; i++)
            {
                if (!arrayTimers[i].hasPlayed && secondsTotal >= arrayTimers[i].seconds && milliseconds >= arrayTimers[i].miliseconds)
                {
                    PlaySonar();
                    arrayTimers[i].hasPlayed = true;
                }
            }
            // PERCORRE array de inimigos para ativar grupos de inimigos de acordo com o tempo e destrui-los também
            for (int j = 0; j < arrayEnemies.Length; j++)
            {
                if (arrayEnemies[j].enemies != null)
                {
                    if (!arrayEnemies[j].enemies.gameObject.activeSelf && seconds >= arrayEnemies[j].secondActive)
                    {
                        arrayEnemies[j].enemies.SetActive(true);
                    }
                    // Temporariamente fora da condição
                    Destroy(arrayEnemies[j].enemies, arrayEnemies[j].secondDestroy);
                }
            }
            //Debug.Log(seconds);
        }
    }


    void Update () {
        
        // Se o jogador toca na tela, ou aperta espaço
        // Deve sumir o gif | Move o player | Tocar a música da fase
        if ( ( (Input.touchCount > 0) || (Input.GetKeyDown(KeyCode.Space)) ) && inTutorial) {
            ImageTouch.SetActive(false); 
            // PARA de tocar o sonar repetidamente
            CancelInvoke("PlaySonar");
            // MOVE PLAYER
            Player.GetComponent<PlayerMovementScript>().enabled = true;
            Player.GetComponent<Rigidbody>().useGravity = true;
            // PLAY NEW MUSIC
            audioSource.clip = PhaseMusic;
            audioSource.Play();
            // Desativa para não entrar novamente, apenas 1 vez
            playerMoving = true;
            inTutorial = false;
        }
    }

    void PlaySonar()
    {
        Player.GetComponent<PlayerScript>().AtivarSonar();
    }

    public void PlayThemeSong(bool status)
    {
        if (!status)
        {
            audioSource.clip = PhaseMusic;
            audioSource.Pause();
        }
        else
        {
            audioSource.clip = PhaseMusic;
            audioSource.Play();
        }
    }




}
