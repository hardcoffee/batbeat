using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveScript : MonoBehaviour {

	public float speed = 50f;
	public int angle = 0;
	private Vector3 minDir;
	public bool stop = false;
	public TrailRenderer trail;

	// Use this for initialization
	void Start ()
    {
		//transform.Rotate (0,angle, 0);
		//Quaternion quat = Quaternion.AngleAxis(angle, transform.forward);
		Quaternion quat = Quaternion.Euler(0, 0, angle);
		minDir = quat * transform.right;
		gameObject.GetComponent<SpriteRenderer>().color = new Vector4 (0.3f, 0.3f, 0.3f, 0.5f);
		trail = gameObject.GetComponentInChildren<TrailRenderer> ();
		trail.receiveShadows = false;
		trail.time = 3;
		trail.startColor = new Vector4(0.3f,0.3f,0.3f,0.5f);
		trail.endColor = new Vector4 (0.3f, 0.3f, 0.3f, 0.5f);
		trail.widthMultiplier = .5f;
    }
	
	// Update is called once per frame
	void Update () {
		if (!stop){
			Vector3 velocity = minDir*speed;
			transform.position += velocity * Time.deltaTime;
        }
	}

    // Colisão do sonar em algo
	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Object")
        {
            other.gameObject.GetComponent<ObjectScript>().collision_raycast(); // Informa que bateu no objeto
            stop = true; // Manda o objeto parar
            trail.Clear();
        }
        if (other.tag == "WallOver")
        {
            stop = true; // Manda o objeto parar
            trail.Clear();
        }
    }


}
