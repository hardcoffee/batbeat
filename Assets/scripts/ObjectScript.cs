﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour {

	// Use this for initialization
    private SpriteRenderer spriteRender;
    private float fadeOutDuration = 3f;
    public bool effectFadeOut = true;

	void Start () {
        spriteRender = GetComponent<SpriteRenderer>();
        if (effectFadeOut)
        {
            spriteRender.color = new Color(1f, 1f, 1f, 0f);
        }
	}
	
	// Update is called once per frame
    void Update()
    {
        if (effectFadeOut)
        {
            // CONTADOR DE TEMPO PARA SUMIR O OBJETO GRADATIVAMENTE
            SpriteRenderer spriteRender = this.gameObject.GetComponent<SpriteRenderer>();
            if (spriteRender.color.a > 0)
            {
                Color color = spriteRender.color;
                spriteRender.color = new Color(color.r, color.g, color.b, color.a - Time.deltaTime * (1 / fadeOutDuration));
            }
        }

        if (PlayerPrefs.GetInt("GameOver") == 1) {
            spriteRender.color = Color.white;
        }
	}

    public void collision_raycast()
    {
        SpriteRenderer spriteRender = this.gameObject.GetComponent<SpriteRenderer>();
        spriteRender.color = new Color(1f, 1f, 1f, 1f);
    }

}
