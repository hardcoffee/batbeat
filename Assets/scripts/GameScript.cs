﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScript : MonoBehaviour {

    //=================//
    // ** UI CANVAS** //
    //===============//
    public GameObject GameWinUI;
    public GameObject GameOverUI;
    public GameObject GameCredits;
    public float levelStartDelay = 2f; // Transição entre cenas
    public int NextSceneNumber;
    //=================//
    // ** SOUNDS ** //
    //===============//
    private AudioSource audioSource;
    public AudioClip GameWinSound;
    public AudioClip GameOverSound;
    //=================//
    // ** SCRIPTS ** //
    //===============//
    public GameObject Player; Rigidbody PlayerR;
    public GameObject Camera; CameraTracksScript CameraS;
    //==================//
    // ** COMPONENTS ** //
    //==================//
    public GameObject PauseView;
    

    void Awake() {
        PlayerR = Player.GetComponent<Rigidbody>();
        CameraS = Camera.GetComponent<CameraTracksScript>();
    }
	// Update is called once per frame
	void Update () {
		
	}
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        PlayerPrefs.SetInt("GameOver", 0);
    }
    public void GameOver()
    {
        GameOverUI.SetActive(true);
        GameWinUI.SetActive(false);
        PlayerPrefs.SetInt("GameOver", 1);
        audioSource.clip = GameOverSound;
        audioSource.Play();
        CameraS.enabled = false;
        Invoke("Restart", 3f); // Esconde a ImageUI em alguns segundos
    }
    public void GameWin()
    {
        GameWinUI.SetActive(true);
        GameOverUI.SetActive(false);
        audioSource.clip = GameWinSound;
        audioSource.Play();
        CameraS.enabled = false;
        Player.GetComponent<PlayerScript>().ScaleMask();
        Invoke("NextScene", 5f); // Esconde a ImageUI em alguns segundos
    }

    private void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private void NextScene()
    {
        if (NextSceneNumber == 0)
        {
            GameCredits.GetComponent<UIView>().Show();
            PauseView.gameObject.SetActive(false);
            GameWinUI.SetActive(false);
        } else {
            if (GameManagerScript.Instance)
            {
                GameManagerScript.Instance.CheckFase(NextSceneNumber);
                SceneManager.LoadScene("scene_" + NextSceneNumber);
            } else
            {
                print("erro ao carregar instancia");
            }
        }
    }

    private void LoadMenu()
    {
        SceneManager.LoadScene("main_menu");
    }

}
