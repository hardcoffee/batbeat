﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskScaler : MonoBehaviour {

    public float scaleSpeed;

    private bool _scaling = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_scaling ) {
            if ( transform.localScale.x > 0 ) {
                transform.localScale -= Vector3.one * scaleSpeed * Time.deltaTime;
            } else {
                transform.localScale = Vector3.zero;
            }
            
        }
	}

    public void ScaleMask() {
        _scaling = true;
    }
}
