﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public GameScript GameScript;
    private AudioSource AudioSource;
    public AudioClip PlayerColision;
    public MaskScaler mask;
    public GameObject maskObject;

    private SonarScript _sonar;
    private Rigidbody _rigidBody;
    public bool IsPlayerDead = false;
    public bool HasPlayerWon = false;

    void Start() {
        // Inicialmente desabilita o radar
        AudioSource = GetComponent<AudioSource>();
        _rigidBody = GetComponent<Rigidbody>();
        _sonar = GetComponent<SonarScript>();
        DesativarSonar();
    }

    // Update is called once per frame
    void Update() {
        if ( IsPlayerDead ) {
            _rigidBody.velocity = Vector2.zero;
        }
    }
    // Colisão com outros elementos 2D
    private void OnTriggerEnter(Collider other) {
        if ( other.tag == "Object" ) { // Final de jogo // Da game over
            if ( !IsPlayerDead && !HasPlayerWon ) {
                AudioSource.clip = PlayerColision;
                AudioSource.Play();
                maskObject.SetActive(true);
                GameScript.GameOver();
                other.GetComponent<SpriteRenderer>().color = Color.white;
                ScaleMask();
                IsPlayerDead = true;
                GetComponent<BoxCollider>().enabled = false;
                GetComponent<PlayerMovementScript>().enabled = false;
                _rigidBody.isKinematic = true;
            }
        }
        if ( other.tag == "Cave" )
        {
            GetComponent<PlayerMovementScript>().enabled = false;
            _rigidBody.isKinematic = true;
            ScaleMask();
            maskObject.SetActive(true);
            GameScript.GameWin();
            HasPlayerWon = true;
        }
        if ( other.tag == "WallOver" )
        {
            GetComponent<PlayerMovementScript>().enabled = false;
            _rigidBody.isKinematic = true;
            ScaleMask();
            IsPlayerDead = true;
            maskObject.SetActive(true);
            GameScript.GameOver();
        }
    }

    public void ScaleMask() {
        if (mask)
        {
            mask.ScaleMask();
        }
    }

    public bool IsDead() {
        return IsPlayerDead;
    }

    public void AtivarSonar() {
        _sonar.visibleLines = true;
    }

    public void DesativarSonar() {
        _sonar.visibleLines = false;
    }

    public void ResetSonar() {
        _sonar.ResetSonars();
    }
}
