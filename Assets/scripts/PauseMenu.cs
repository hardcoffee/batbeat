﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject panelPauseUI;
    public GameObject buttonPauseUI;
    private SceneCtrl gameController;

    private bool paused = false;
    void Start()
    {
        gameController = this.GetComponent<SceneCtrl>();
        //pauseUI.SetActive(false); // Inicialmente o game não tem o menu-pause liberado
        buttonPauseUI.SetActive(true);
    }

    void Update()
    {
        //if (Input.GetButtonDown("Esc"))
        //{
        //    paused = !paused;

        //    if (paused)
        //    {
        //        panelPauseUI.SetActive(true);
        //        gameController.PlayThemeSong(false);
        //        Time.timeScale = 0;
        //    }
        //    if (!paused)
        //    {
        //        panelPauseUI.SetActive(false);
        //        gameController.PlayThemeSong(true);
        //        Time.timeScale = 1;
        //    }
        //}
    }

    public void ResumeGame()
    {
        // Continua a música (SE já tiver sido clicado na tela)
        if (!gameController.inTutorial)
        {
            gameController.PlayThemeSong(true);
        }
        // Se clicar, independente. Retira a tela de cima
        panelPauseUI.SetActive(false);
        buttonPauseUI.SetActive(true);
        Time.timeScale = 1;
    }
    public void PauseGame()
    {
        // Pausa a música
        panelPauseUI.SetActive(true);
        gameController.PlayThemeSong(false);
        Time.timeScale = 0;
    }

    public void RestartGame(string scenename)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(scenename);
    }
    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("main_menu");
    }
}
