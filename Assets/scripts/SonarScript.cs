﻿using System.Collections.Generic;
using UnityEngine;

public class SonarScript : MonoBehaviour
{

    public bool visibleLines = false;
    public bool isMenu = false;
    public int speed_wave = 0;
    public GameObject ProjetilPrefab;
    public float countdown = 2.0f;

    private float countdownAux;

    // SONAR //
    private int iterator = 0;
    private List<List<GameObject>> sonares;

    void Start() {
        countdownAux = countdown; // Define o auxiliar inicialmente com o valor primário
        sonares = new List<List<GameObject>>();
        // Cria a lista de sonares
        for ( int sonar = 0; sonar < 5; sonar++ ) {
            sonares.Add(new List<GameObject>());
            // Sonar
            for ( int angulo = -90; angulo <= 90; angulo += 5 ) {
                // Sonar 1
                sonares[sonar].Add(Instantiate(ProjetilPrefab, transform.position, transform.rotation));

                int elementIndex = sonares[sonar].Count - 1;
                sonares[sonar][elementIndex].GetComponent<WaveScript>().speed = speed_wave;
                sonares[sonar][elementIndex].GetComponent<WaveScript>().angle = angulo;
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if ( visibleLines ) {
            //print("iterator : " + iterator);
            if (sonares[iterator]!=null)
            {
                for (int i = 0; i < sonares[iterator].Count; i++)
                {
                    sonares[iterator][i].GetComponentInChildren<TrailRenderer>().enabled = false;
                    sonares[iterator][i].transform.position = transform.position; // Retorna o Sonar para a posição do player
                    sonares[iterator][i].GetComponent<WaveScript>().stop = false;
                    sonares[iterator][i].GetComponent<WaveScript>().trail.Clear();
                    sonares[iterator][i].GetComponentInChildren<TrailRenderer>().enabled = true;
                }
            }
            // Manda todos as waves se mover
            visibleLines = false; // Desativa o sonar e para de emitir
            iterator++;
            iterator %= 5;
        }
        //Se for o menu, toca a cada 1seg
        if ( isMenu ) {
            countdownAux -= Time.deltaTime;
            if ( countdownAux < 0 ) {
                visibleLines = true;
                countdownAux = countdown;
            }
        }
    }

    public void ResetSonars() {
        for ( int sonar = 0; sonar < 5; sonar++ ) {
            for ( int i = 0; i < sonares[sonar].Count; i++ ) {
                sonares[sonar][i].transform.position = transform.position; // Retorna o Sonar para a posição do player
                sonares[sonar][i].GetComponent<WaveScript>().stop = true;
                sonares[sonar][i].GetComponent<WaveScript>().trail.Clear();
            }
        }
    }


}
