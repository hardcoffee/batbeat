﻿// 2016 - Damien Mayance (@Valryon)
// Source: https://github.com/valryon/water2d-unity/
using UnityEngine;
using UnityEngine.UIElements;

/// <summary>
/// Automagically create a water reflect for a sprite.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class WaterReflectableScript : MonoBehaviour
{
    #region Members

    [Header("Reflect properties")]
    public Vector3 localPosition = new Vector3(0, 0, 0);
    public Vector3 localRotation = new Vector3(180, 0, 0);
    [Tooltip("Optionnal: force the reflected sprite. If null it will be a copy of the source.")]
    public Sprite sprite;
    public string spriteLayer = "Default";
    public int spriteLayerOrder = -5;
    public GameObject ReflectGroup;
    public bool DinamicPosition = false;

    private GameObject _BatReflect;
    private Vector3 _ObjectStartPosition;
    private Vector3 _ObjectReflectStartPosition;
    private SpriteRenderer spriteSource;
    private SpriteRenderer spriteRenderer;

    #endregion

  #region Timeline

    void Awake()
    {
        GameObject reflectGo = new GameObject("Water Reflect");
        reflectGo.transform.parent = ReflectGroup.transform;
        reflectGo.transform.localPosition = localPosition;
        reflectGo.transform.localRotation = Quaternion.Euler(localRotation);
        reflectGo.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);

        spriteRenderer = reflectGo.AddComponent<SpriteRenderer>();
        spriteRenderer.sortingLayerName = spriteLayer;
        spriteRenderer.sortingOrder = spriteLayerOrder;
        
        spriteSource = GetComponent<SpriteRenderer>();
        reflectGo.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask; // Adicionado para interagir com máscara de sprite

        _ObjectStartPosition = this.transform.position;
        _ObjectReflectStartPosition = reflectGo.transform.position;
        _BatReflect = reflectGo;
    }

    void OnDestroy()
    {
        if (spriteRenderer != null)
        {
            Destroy(spriteRenderer.gameObject);
        }
    }

  void LateUpdate()
  {
    if (spriteSource != null)
    {
        if (sprite == null) {
            spriteRenderer.sprite = spriteSource.sprite;
        } else {
            spriteRenderer.sprite = sprite;
        }
        spriteRenderer.flipX = spriteSource.flipX;
        spriteRenderer.flipY = spriteSource.flipY;
        spriteRenderer.color = spriteSource.color;

        if (DinamicPosition)
        {
            float _diference = this.transform.position.y - _ObjectStartPosition.y;
            float _sum = _ObjectReflectStartPosition.y + _diference;
            _BatReflect.transform.position = new Vector3(_BatReflect.transform.position.x, _ObjectReflectStartPosition.y - _sum);
        }
    }
  }

  #endregion
}