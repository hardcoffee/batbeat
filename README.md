## What's about?
** BatBeat is a game made it for Global Game Jam (GGJ) 2017, who use the theme "Waves". The entire game is autoral, like code, music and art. **

GGJ Site: https://globalgamejam.org/2017/games/bat-blind

Sinopse:
Running to the cave, escaping from the sun

---
## Structure

Above, the scenes and screens who made the game:

1. Menu
2. Tutorial
3. Scene 1: Bosque
4. Scene 2: Rua
5. Scene 3: Residencial
6. Scene 4: Floresta
7. Scene 5: Bosque da Lua
8. Scene 6: Lago
9. Scene 7: Vale
10. Scene 8: Cemitério

---
## Next steps

What we'll do with the game?

1. Complete the basic cycle with the structures
2. Publish in website of Hardcoffee Game Studio
3. Publish in GooglePlay
3. Publish in AppleStore
---
## Sugestions?

Send an email to us: hardcoffeestudio@gmail.com